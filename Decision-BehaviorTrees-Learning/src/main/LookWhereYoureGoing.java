package main;

public class LookWhereYoureGoing {
	public static Steering getSteering(Character c) {
		double targetOrientation = c.getOrientation();
		
		if (c.getVelocity().mag() >= 0.05) {
			targetOrientation = Math.atan2(c.getVelocity().x, c.getVelocity().y);			
		}
		
		return Align.getSteering(c, targetOrientation);
	}
}
