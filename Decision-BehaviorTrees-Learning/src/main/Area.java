package main;
import java.util.Random;

import processing.core.PVector;

public enum Area {
	HALLWAY, ROOM1, ROOM2, ROOM3, ROOM4, ROOM5, ROOM6, ROOM7, ROOM8, ROOM9,
						ROOM10, ROOM11, ROOM12, ROOM13, ROOM14, ROOM15, ROOM16, ROOM17, ROOM18;
	
	private static final int BUFFER = 2;
	private static final PVector ROOM1START = new PVector(3, 3);
	private static final PVector ROOM1END = new PVector(401, 101);
	private static final PVector ROOM2START = new PVector(503, 3);
	private static final PVector ROOM2END = new PVector(796, 101);
	private static final PVector ROOM3START = new PVector(3, 103);
	private static final PVector ROOM3END = new PVector(401, 201);
	private static final PVector ROOM4START = new PVector(503, 103);
	private static final PVector ROOM4END = new PVector(796, 201);
	private static final PVector ROOM5START = new PVector(3, 203);
	private static final PVector ROOM5END = new PVector(401, 301);
	private static final PVector ROOM6START = new PVector(503, 203);
	private static final PVector ROOM6END = new PVector(796, 303);
	private static final PVector ROOM7START = new PVector(3, 303);
	private static final PVector ROOM7END = new PVector(401, 351);
	private static final PVector ROOM8START = new PVector(503, 303);
	private static final PVector ROOM8END = new PVector(796, 351);
	private static final PVector ROOM9START = new PVector(3, 353);
	private static final PVector ROOM9END = new PVector(301, 451);
	private static final PVector ROOM10START = new PVector(503, 353);
	private static final PVector ROOM10END = new PVector(796, 451);
	private static final PVector ROOM11START = new PVector(3, 453);
	private static final PVector ROOM11END = new PVector(301, 551);
	private static final PVector ROOM12START = new PVector(403, 453);
	private static final PVector ROOM12END = new PVector(796, 551);
	private static final PVector ROOM13START = new PVector(3, 553);
	private static final PVector ROOM13END = new PVector(301, 651);
	private static final PVector ROOM14START = new PVector(403, 553);
	private static final PVector ROOM14END = new PVector(796, 651);
	private static final PVector ROOM15START = new PVector(3, 653);
	private static final PVector ROOM15END = new PVector(301, 751);
	private static final PVector ROOM16START = new PVector(403, 653);
	private static final PVector ROOM16END = new PVector(796, 751);
	private static final PVector ROOM17START = new PVector(3, 753);
	private static final PVector ROOM17END = new PVector(301, 796);
	private static final PVector ROOM18START = new PVector(403, 753);
	private static final PVector ROOM18END = new PVector(796, 796);

	private static Random rand = new Random();

	public static Area getAreaFromLocation(PVector loc) {
		if (loc.x >= ROOM1START.x && loc.x <= ROOM1END.x && loc.y >= ROOM1START.y && loc.y <= ROOM1END.y) {
			return Area.ROOM1;
		} else if (loc.x >= ROOM2START.x && loc.x <= ROOM2END.x && loc.y >= ROOM2START.y && loc.y <= ROOM2END.y) {
			return Area.ROOM2;
		} else if (loc.x >= ROOM3START.x && loc.x <= ROOM3END.x && loc.y >= ROOM3START.y && loc.y <= ROOM3END.y) {
			return Area.ROOM3;
		} else if (loc.x >= ROOM4START.x && loc.x <= ROOM4END.x && loc.y >= ROOM4START.y && loc.y <= ROOM4END.y) {
			return Area.ROOM4;
		} else if (loc.x >= ROOM5START.x && loc.x <= ROOM5END.x && loc.y >= ROOM5START.y && loc.y <= ROOM5END.y) {
			return Area.ROOM5;
		} else if (loc.x >= ROOM6START.x && loc.x <= ROOM6END.x && loc.y >= ROOM6START.y && loc.y <= ROOM6END.y) {
			return Area.ROOM6;
		} else if (loc.x >= ROOM7START.x && loc.x <= ROOM7END.x && loc.y >= ROOM7START.y && loc.y <= ROOM7END.y) {
			return Area.ROOM7;
		} else if (loc.x >= ROOM8START.x && loc.x <= ROOM8END.x && loc.y >= ROOM8START.y && loc.y <= ROOM8END.y) {
			return Area.ROOM8;
		} else if (loc.x >= ROOM9START.x && loc.x <= ROOM9END.x && loc.y >= ROOM9START.y && loc.y <= ROOM9END.y) {
			return Area.ROOM9;
		} else if (loc.x >= ROOM10START.x && loc.x <= ROOM10END.x && loc.y >= ROOM10START.y && loc.y <= ROOM10END.y) {
			return Area.ROOM10;
		} else if (loc.x >= ROOM11START.x && loc.x <= ROOM11END.x && loc.y >= ROOM11START.y && loc.y <= ROOM11END.y) {
			return Area.ROOM11;
		} else if (loc.x >= ROOM12START.x && loc.x <= ROOM12END.x && loc.y >= ROOM12START.y && loc.y <= ROOM12END.y) {
			return Area.ROOM12;
		} else if (loc.x >= ROOM13START.x && loc.x <= ROOM13END.x && loc.y >= ROOM13START.y && loc.y <= ROOM13END.y) {
			return Area.ROOM13;
		} else if (loc.x >= ROOM14START.x && loc.x <= ROOM14END.x && loc.y >= ROOM14START.y && loc.y <= ROOM14END.y) {
			return Area.ROOM14;
		} else if (loc.x >= ROOM15START.x && loc.x <= ROOM15END.x && loc.y >= ROOM15START.y && loc.y <= ROOM15END.y) {
			return Area.ROOM15;
		} else if (loc.x >= ROOM16START.x && loc.x <= ROOM16END.x && loc.y >= ROOM16START.y && loc.y <= ROOM16END.y) {
			return Area.ROOM16;
		} else if (loc.x >= ROOM17START.x && loc.x <= ROOM17END.x && loc.y >= ROOM17START.y && loc.y <= ROOM17END.y) {
			return Area.ROOM17;
		} else if (loc.x >= ROOM18START.x && loc.x <= ROOM18END.x && loc.y >= ROOM18START.y && loc.y <= ROOM18END.y) {
			return Area.ROOM18;
		} else {
			return Area.HALLWAY;
		}
	}
	
	public static PVector getRandomLocationInArea(Area area) {
		float lowX, highX, lowY, highY;
		switch (area) {
		case HALLWAY:
			break;
		case ROOM1:
			lowX = ROOM1START.x + BUFFER;
			highX = ROOM1END.x - BUFFER;
			lowY = ROOM1START.y + BUFFER;
			highY = ROOM1END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM2:
			lowX = ROOM2START.x + BUFFER;
			highX = ROOM2END.x - BUFFER;
			lowY = ROOM2START.y + BUFFER;
			highY = ROOM2END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM3:
			lowX = ROOM3START.x + BUFFER;
			highX = ROOM3END.x - BUFFER;
			lowY = ROOM3START.y + BUFFER;
			highY = ROOM3END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM4:
			lowX = ROOM4START.x + BUFFER;
			highX = ROOM4END.x - BUFFER;
			lowY = ROOM4START.y + BUFFER;
			highY = ROOM4END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM5:
			lowX = ROOM5START.x + BUFFER;
			highX = ROOM5END.x - BUFFER;
			lowY = ROOM5START.y + BUFFER;
			highY = ROOM5END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM6:
			lowX = ROOM6START.x + BUFFER;
			highX = ROOM6END.x - BUFFER;
			lowY = ROOM6START.y + BUFFER;
			highY = ROOM6END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM7:
			lowX = ROOM7START.x + BUFFER;
			highX = ROOM7END.x - BUFFER;
			lowY = ROOM7START.y + BUFFER;
			highY = ROOM7END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM8:
			lowX = ROOM8START.x + BUFFER;
			highX = ROOM8END.x - BUFFER;
			lowY = ROOM8START.y + BUFFER;
			highY = ROOM8END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM9:
			lowX = ROOM9START.x + BUFFER;
			highX = ROOM9END.x - BUFFER;
			lowY = ROOM9START.y + BUFFER;
			highY = ROOM9END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM10:
			lowX = ROOM10START.x + BUFFER;
			highX = ROOM10END.x - BUFFER;
			lowY = ROOM10START.y + BUFFER;
			highY = ROOM10END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM11:
			lowX = ROOM11START.x + BUFFER;
			highX = ROOM11END.x - BUFFER;
			lowY = ROOM11START.y + BUFFER;
			highY = ROOM11END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM12:
			lowX = ROOM12START.x + BUFFER;
			highX = ROOM12END.x - BUFFER;
			lowY = ROOM12START.y + BUFFER;
			highY = ROOM12END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM13:
			lowX = ROOM13START.x + BUFFER;
			highX = ROOM13END.x - BUFFER;
			lowY = ROOM13START.y + BUFFER;
			highY = ROOM13END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM14:
			lowX = ROOM14START.x + BUFFER;
			highX = ROOM14END.x - BUFFER;
			lowY = ROOM14START.y + BUFFER;
			highY = ROOM14END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM15:
			lowX = ROOM15START.x + BUFFER;
			highX = ROOM15END.x - BUFFER;
			lowY = ROOM15START.y + BUFFER;
			highY = ROOM15END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM16:
			lowX = ROOM16START.x + BUFFER;
			highX = ROOM16END.x - BUFFER;
			lowY = ROOM16START.y + BUFFER;
			highY = ROOM16END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM17:
			lowX = ROOM17START.x + BUFFER;
			highX = ROOM17END.x - BUFFER;
			lowY = ROOM17START.y + BUFFER;
			highY = ROOM17END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);
		case ROOM18:
			lowX = ROOM18START.x + BUFFER;
			highX = ROOM18END.x - BUFFER;
			lowY = ROOM18START.y + BUFFER;
			highY = ROOM18END.y - BUFFER;
			return getRandomLocationWithinCoords(lowX, lowY, highX, highY);		
		default:
			break;
		
		}
		return null;
	}
	
	private static PVector getRandomLocationWithinCoords(float lowX, float lowY, float highX, float highY) {
		float randX = rand.nextFloat() * (highX - lowX);
		randX += lowX;
		float randY = rand.nextFloat() * (highY - lowY);
		randY += lowY;
		return new PVector(randX, randY);
	}
}