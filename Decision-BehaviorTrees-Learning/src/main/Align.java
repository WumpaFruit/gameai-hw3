package main;
public class Align {
	private static final float TARGET_ROTATION_RADIUS = 0.1f;
	private static final float SLOW_ROTATION_RADIUS = 1f;
	private static final float TIME_TO_TARGET = .1f;
	
	public static Steering getSteering(Character c, double targetOrientation) {
		Steering s = new Steering();
		
		//Get naive rotation to target
		double rotation = targetOrientation - c.getOrientation();
		
		//Map result to (-pi, pi)
		rotation = mapToRange(rotation);
		double rotationSize = Math.abs(rotation);
		
		double targetRotation = 0;
		
		if (rotationSize > TARGET_ROTATION_RADIUS) {
			if (rotationSize > SLOW_ROTATION_RADIUS) {
				targetRotation = c.getMaxRotation();
			} else { //Calculate a scaled rotation
				targetRotation = c.getMaxRotation() * rotationSize / SLOW_ROTATION_RADIUS;
			}
			
			targetRotation *= Math.signum(rotation);
		} else {
			return null;
		}
		
		s.angular = (float) (targetRotation - c.getRotation());
		s.angular /= TIME_TO_TARGET;		
		
		return s;
	}

	private static double mapToRange(double rotation) {
		while (rotation < -Math.PI || rotation > Math.PI) {
			if (rotation < -Math.PI) {
				rotation += Math.PI * 2;
			} else {
				rotation -= Math.PI * 2;
			}
		}
		
		return rotation;
	}
}
