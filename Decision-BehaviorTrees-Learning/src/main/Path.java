package main;
import java.util.ArrayList;

public class Path {
	private ArrayList<Vertex> vertices;
	
	public Path() {
		vertices = new ArrayList<>();
	}
	
	public void addVertex(Vertex v) {
		vertices.add(v);
	}
	
	public void addVertexToBeginning(Vertex v) {
		vertices.add(0, v);
	}
	
	public boolean isEmpty() {
		return vertices.isEmpty();
	}
	
	public Vertex getFirstVertex() {
		return vertices.get(0);
	}
	
	public void removeFirstVertex() {
		vertices.remove(0);
	}
	
	public ArrayList<Vertex> getVertices() {
		return vertices;
	}
	
	public String toString() {
		String s = "";
		
		for (int i = 0; i < vertices.size(); i++) {
			if (i < vertices.size() - 1) {
				s += vertices.get(i) + "-->";
			} else {
				s += vertices.get(i) + ".";
			}
		}
		
		return s;
	}
}
