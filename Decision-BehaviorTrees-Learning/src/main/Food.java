package main;
import processing.core.PVector;


public class Food extends Tile {
	/** Is this piece of food available for pickup? **/
	private boolean available;

	public Food(int x, int y, int width, int height) {
		super(x, y, width, height);
	}
	
	public void setPosition(PVector loc) {
		setX((int)loc.x);
		setY((int)loc.y);
	}
	
	@Override
	public void setObstacle(boolean isObstacle) {
		super.setObstacle(isObstacle);
		
		available = isObstacle; //If food is an obstacle, it is available for pickup
	}
	
	public boolean isAvailable() {
		return available;
	}
}