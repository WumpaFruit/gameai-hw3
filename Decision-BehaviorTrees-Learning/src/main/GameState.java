package main;

import java.util.ArrayList;
import java.util.Random;

import behaviortree.monster.dance.RepeatDecorator;
import processing.core.PVector;

public class GameState {
	public static final float MONSTER_LOW_STAMINA_LEVEL = 25;
	
	public PVector wanderTarget, closestFoodTarget;
	public LineSegmentPath wanderPath, closestFoodPath;
	public PVector perceptionWindowLeft, perceptionWindowRight;
	public Food closestFood;
	public Area area;
	public Random rand;
	public Steering steering, pathFollowWanderSteering, pathFollowToFoodSteering, seekHeroSteering, alignOrientationSteering;
	public Character character, hero;
	public String status;
	public ArrayList<Food> availableFoodItems;
	public boolean hostile, danceOrientationsGenerated;
	public long chaseTimer, dancePauseTimer, danceTimer;
	public boolean monsterCelebrating;
	public ArrayList<Double> danceOrientations;
	public int run;	
	public boolean seekHeroSteeringGenerated, seekFoodSteeringGenerated, wanderSteeringGenerated;
	
	public String getHeaderString() {
		String s = "";
		
		s += "staminaLow,foodAvailable,pathFollowToFoodSteering,wanderTarget,wanderPath,pathFollowWanderSteering,closestFoodTarget,closestFoodPath,hostile,seekHeroSteering,monsterCelebrating,danceOrientationsGenerated,alignOrientationSteering,danceTimerSet,dancePauseTimerSet,dancePauseCompleted,danceCompleted";
		
		s += ",action\n";
		
		return s;
	}
	
	public String getOutputString() {
		String s = "";
		
		s += staminaLow() + "," + foodAvailable() + "," + (pathFollowToFoodSteering != null) 
				+ "," + (wanderTarget != null) + "," + (wanderPath != null) + "," 
				+ (pathFollowWanderSteering != null) + "," + (closestFoodTarget != null) + "," 
				+ (closestFoodPath != null) + "," + hostile + "," + (seekHeroSteering != null) 
				+ "," + monsterCelebrating + "," + (alignOrientationSteering != null) + "," 
				+ danceOrientationsGenerated + "," + danceTimerSet() + "," + dancePauseTimerSet() 
				+ "," + dancePauseCompleted() + "," + danceCompleted() + "," + status + "\n";
		
		return s;
	}
	
	public boolean staminaLow() {
		return character.getStamina() <= MONSTER_LOW_STAMINA_LEVEL;
	}
	
	public boolean foodAvailable() {
		return availableFoodItems.size() > 0;
	}
	
	public boolean dancePauseTimerSet() {
		return dancePauseTimer != 0L;
	}
	
	public boolean danceTimerSet() {
		return danceTimer != 0L;
	}
	
	public boolean dancePauseCompleted() {
		return System.currentTimeMillis() - dancePauseTimer >= RepeatDecorator.PAUSE_TIME;
	}
	
	public boolean danceCompleted() {
		return System.currentTimeMillis() - danceTimer >= RepeatDecorator.DANCE_TIME;
	}
}
