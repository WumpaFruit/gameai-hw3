package main;
import processing.core.PApplet;
import processing.core.PVector;


public class LineSegment {
	private PVector start;
	private PVector end;
	
	public LineSegment(PVector start, PVector end) {
		this.start = start;
		this.end = end;
	}
	
	public void draw(PApplet parent) {
		parent.line(start.x, start.y, end.x, end.y);
	}
	
	public double getLength() {
		float deltaX = start.x - end.x;
		float deltaY = start.y - end.y;
		
		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}

	public PVector getStart() {
		return start;
	}

	public PVector getEnd() {
		return end;
	}
	
	
}
