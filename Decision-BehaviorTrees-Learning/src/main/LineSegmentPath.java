package main;
import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;


public class LineSegmentPath {
	private ArrayList<LineSegment> path;
	
	public LineSegmentPath() {
		path = new ArrayList<>();
	}
	
	public void addToBeginning(LineSegment seg) {
		path.add(0, seg);
	}
	
	public void add(LineSegment seg) {
		path.add(seg);
	}
	
	public int size() {
		return path.size();
	}
	
	public void draw(PApplet parent) {
		for (int i = 0; i < path.size(); i++) {
			path.get(i).draw(parent);
		}
	}
	
	public PVector getPosition(float param) {
		if (param < 0) {
			throw new IllegalArgumentException("Parameter cannot be negative");
		}
		
		//Begin with the first line segment and loop until we have found the correct line segment
		int i = 0;
		LineSegment seg = path.get(i);
		double start = 0;
		double end = start + seg.getLength();
		while (param > end) {
			i++;
			if (i >= path.size()) {
				return null;
			}
			seg = path.get(i);
			start = end;
			end = start + seg.getLength();
		};
		
		//If seg is not null, the position should be somewhere along seg
		double startToParam = param - start;
		double fractionalDistance = startToParam / seg.getLength();
		
		//Now that we have the fractional distance, compute the positions
		double deltaX = seg.getEnd().x - seg.getStart().x;
		double deltaY = seg.getEnd().y - seg.getStart().y;
		
		//Return the start of the segment plus the fractional distance of the total distance from start to end
		return new PVector((float)(seg.getStart().x + deltaX * fractionalDistance), (float)(seg.getStart().y + deltaY * fractionalDistance));
	}
	
	//Adapted from http://stackoverflow.com/questions/1459368/snap-point-to-a-line
	public PVector snapToClosestLineSegment(PVector p) {
		float closestDistance = Float.MAX_VALUE;
		PVector closestPoint = null;
		
		for (int i = 0; i < path.size(); i++) {
			LineSegment seg = path.get(i);
			double ax = seg.getStart().x;
			double ay = seg.getStart().y;
			double bx = seg.getEnd().x;
			double by = seg.getEnd().y;
			
			double apx = p.x - ax;
			double apy = p.y - ay;
			double abx = bx - ax;
			double aby = by - ay;
			
			double ab2 = abx * abx + aby * aby;
			double ap_ab = apx * abx + apy * aby;
			double t = ap_ab / ab2;
			
			if (t < 0) {
				t = 0;
			}
			
			if (t > 1) {
				t = 1;
			}
			
			PVector snapPoint = new PVector((float)(ax + abx * t), (float)(ay + aby * t));
			float distance = PVector.dist(snapPoint, p);
			
			if (distance < closestDistance) {
				closestDistance = distance;
				closestPoint = snapPoint;
			}
		}
		
		return closestPoint;
	}
	
	public float getParam(PVector pos) {
		float accumulatedParam = 0;
		LineSegment endSegment = null;
		for (int i = 0; i < path.size(); i++) {
			LineSegment curr = path.get(i);
			//If point is on the given line segment, determine where it is,
			//otherwise add the entire length of the segment
			if (PVector.dist(curr.getStart(), pos) + PVector.dist(pos, curr.getEnd()) - curr.getLength() <= .01) {
				endSegment = curr;
				break;
			} else {
				accumulatedParam += curr.getLength();
			}
		}
		if (endSegment == null) {
			return Float.MAX_VALUE;
		} else {
			accumulatedParam += PVector.dist(endSegment.getStart(), pos);
			return accumulatedParam;
		}
	}
}
