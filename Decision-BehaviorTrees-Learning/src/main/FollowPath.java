package main;
import processing.core.PVector;


public class FollowPath {
	private static final float PREDICT_TIME = 0.1f;
	
	public static Steering getSteering(Character c, LineSegmentPath path) {				
		if (path == null) {
			return null;
		}
		
		//Calculate expected future displacement of the character
		float offsetParam = PVector.mult(c.getVelocity(), PREDICT_TIME).mag();
		
		//Snap current location of character to path
		float currentParam = path.getParam(path.snapToClosestLineSegment(c.getPosition()));
		
		if (currentParam == Float.MAX_VALUE) {
			return null;
		}
		
		//Offset currentParam
		float targetParam = currentParam + offsetParam;
		
		PVector targetPos = path.getPosition(targetParam);
		//Reached end of path
		if (targetPos == null) {
			return null;
		}
		
		return Seek.getSteering(c, targetPos);
	}
}
