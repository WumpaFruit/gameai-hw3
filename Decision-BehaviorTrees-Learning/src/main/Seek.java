package main;
import processing.core.PVector;


public class Seek {
	public static Steering getSteering(Character c, PVector target) {
		if (target == null || PVector.dist(c.getPosition(), target) <= 2) {
			return null;
		}
		
		Steering s = new Steering();
		
		s.linear = PVector.sub(target, c.getPosition());
		
		s.linear.normalize();
		s.linear = PVector.mult(s.linear, (float) c.getMaxAcceleration());
		s.angular = 0;
		
		return s;
	}
}
