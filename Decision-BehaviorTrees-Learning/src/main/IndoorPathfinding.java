package main;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;

import processing.core.PApplet;
import processing.core.PVector;

@SuppressWarnings("serial")
public class IndoorPathfinding extends PApplet {
	private static final String OUTPUT_FILENAME = "output.txt";
	private static final boolean LOG_OUTPUT = true;
	private static final boolean RUN_MONSTER_BEHAVIOR_TREE = true;
	private static final boolean TWO_MONSTERS = false;

	private static final int WIDTH = 800;
	private static final int HEIGHT = 800;

	/** Time until food spawns **/
	private static final long FOOD_INTERVAL = 10000;

	private static TileGrid tileGrid;
	private static Character hero, monster, monster2;
	private static final int BREADCRUMB_INTERVAL = 900;
	private static final float MAX_SPEED = 0.05f;
	private static final float RADIUS_OF_SATISFACTION = 5f;
	private static int lastMillis;
	private static LinkedList<Character> breadcrumbs;
	private static int delay;
	private static Color backgroundColor = new Color(255, 255, 255);
	private static Graph g;
	private static boolean showBreadcrumbs, showPath;
	private static long foodTimer;
	private static Food food1, food2;
	private static ArrayList<Food> foodItems;
	private static PVector userDefinedLoc;
	private static boolean resetting;
	private static BufferedWriter writer;

	public static void main(String[] args) {
		PApplet.main(new String[] { "--present", "main.IndoorPathfinding" });
	}

	public void setup() {
		size(WIDTH, HEIGHT);
		lastMillis = millis();
		resetGame();

		if (LOG_OUTPUT) {
			try {
				writer = Files.newBufferedWriter(Paths.get(OUTPUT_FILENAME),
						Charset.forName("US-ASCII"));
				writer.write(((Character.MonsterBehavior) monster.getBehavior()).getGameState().getHeaderString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		prepareExitHandler();
	}

	public void draw() {
		background(backgroundColor.getRed(), backgroundColor.getGreen(),
				backgroundColor.getBlue());

		stroke(0, 0, 255);
		noFill();
		rectMode(CORNER);
		Tile t = tileGrid.get(60, 159);
		t.setColorR((short) 0);
		t.setColorG((short) 0);
		// System.out.println(t.getX()+ " " + t.getY());
		// System.out.println(tileGrid.getFromLocation(6, 106).isObstacle() +
		// " " + tileGrid.getFromLocation(396, 94).isObstacle());

		tileGrid.draw(this, backgroundColor);

		if (userDefinedLoc != null) {
			line(userDefinedLoc.x - 2, userDefinedLoc.y - 2,
					userDefinedLoc.x + 2, userDefinedLoc.y + 2);
			line(userDefinedLoc.x + 2, userDefinedLoc.y - 2,
					userDefinedLoc.x - 2, userDefinedLoc.y + 2);
		}

		if (showBreadcrumbs) {
			for (Character crumb : breadcrumbs) {
				crumb.draw(this, true);
			}
		}
		hero.draw(this, false);
		monster.draw(this, false);
		
		if (TWO_MONSTERS) {
			monster2.draw(this, false);
		}

		textSize(16);

		float heroStamina = hero.getStamina();
		float monsterStamina = monster.getStamina();

		if (heroStamina > 25) {
			fill(0, 0, 0);
		} else {
			fill(255, 0, 0);
		}
		textAlign(RIGHT);
		text("Hero's stamina: " + (int) heroStamina, WIDTH - 10, 20);

		if (monsterStamina > 25) {
			fill(0, 0, 0);
		} else {
			fill(255, 0, 0);
		}
		text("Monster's stamina: " + (int) monsterStamina, WIDTH - 10, 35);
		
		if (TWO_MONSTERS) {
			float monster2Stamina = monster2.getStamina();
			if (monster2Stamina > 25) {
				fill(0, 0, 0);
			} else {
				fill(255, 0, 0);
			}
			text("Decision Monster's stamina: " + (int) monster2Stamina, WIDTH - 10, 50);
		}

		fill(255, 0, 0);
		textAlign(LEFT);

		/*
		 * if (showBreadcrumbs) {
		 * text("Breadcrumbs are enabled - Press B to toggle", 10, HEIGHT - 30);
		 * } else { text("Breadcrumbs are disabled - Press B to toggle", 10,
		 * HEIGHT - 30); }
		 */

		if (!TWO_MONSTERS) {
			text("Hero's state: " + hero.getState(), 10, HEIGHT - 25);
			text("Monster's state: " + monster.getState(), 10, HEIGHT - 10);
		} else {
			text("Hero state: " + hero.getState(), 10, HEIGHT - 40);
			text("Monster's state: " + hero.getState(), 10, HEIGHT - 25);
			text("Decision monster's state: " + monster.getState(), 10, HEIGHT - 10);
		}

		update();
	}

	public void update() {
		int time = millis() - lastMillis;
		monster.update(time);
		
		if (TWO_MONSTERS) {
			monster2.update(time);
		}

		if (!resetting) {
			hero.update(time);

			if (foodTimer > 0 && checkTimer()) {
				placeFood();
				foodTimer = 0;
			}

			// Do not add breadcrumbs when boid is not moving
			if (Vector.doublesEqual(hero.getVelocity().x, 0.0)
					&& Vector.doublesEqual(hero.getVelocity().y, 0.0)) {
				delay += time;
			}

			if (millis() >= (breadcrumbs.size() * BREADCRUMB_INTERVAL) + delay) {
				// System.out.println("Add breadcrumb");
				breadcrumbs.add(new Character(new PVector(hero.getPosition().x,
						hero.getPosition().y), hero.getOrientation(),
						CharacterBehavior.Type.NONE));
			}
		}

		lastMillis = millis();
	}

	public void mousePressed() {
		if (userDefinedLoc == null) {
			userDefinedLoc = new PVector(mouseX, mouseY);
		} else {
			userDefinedLoc = null;
		}

		((Character.HeroBehavior) hero.getBehavior())
				.setUserDefinedLocation(userDefinedLoc);

		/*
		 * long timer = System.currentTimeMillis(); Path p =
		 * AStar.pathfindAStar(g,
		 * g.getVertex(Integer.toString(tileGrid.getTileNumFromLocation
		 * (c.getPosition().x, c.getPosition().y))),
		 * g.getVertex(Integer.toString(tileGrid.getTileNumFromLocation(mouseX,
		 * mouseY))), AStarHeuristics.Type.MANHATTAN, true).path; if (p != null)
		 * { c.setPath(generateSegmentPath(p)); } lastMillis +=
		 * System.currentTimeMillis() - timer;
		 */
	}

	public static void arrivedAtUserDefinedPath() {
		userDefinedLoc = null;
	}

	public static LineSegmentPath pathfindAStar(PVector start, PVector goal) {
		LineSegmentPath path = null;
		long timer = System.currentTimeMillis();
		Path p = AStar
				.pathfindAStar(g, g.getVertex(Integer.toString(tileGrid
						.getTileNumFromLocation(start.x, start.y))), g
						.getVertex(Integer.toString(tileGrid
								.getTileNumFromLocation(goal.x, goal.y))),
						AStarHeuristics.Type.MANHATTAN, true).path;
		if (p != null) {
			path = generateSegmentPath(p);
		}
		lastMillis += System.currentTimeMillis() - timer;

		return path;
	}

	public void keyPressed() {
		if (key == 'b') {
			showBreadcrumbs = !showBreadcrumbs;
		}

		if (key == 'p') {
			showPath = !showPath;
		}
	}

	public static LineSegmentPath generateSegmentPath(Path p) {
		LineSegmentPath path = new LineSegmentPath();
		for (int i = 0, j = 1; j < p.getVertices().size(); i++, j++) {
			Vertex current = p.getVertices().get(i);
			Vertex next = p.getVertices().get(j);
			int currentTileNum = Integer.parseInt(current.getName());
			PVector currentPos = tileGrid.getLocationOfTile(currentTileNum);

			int nextTileNum = Integer.parseInt(next.getName());
			PVector nextPos = tileGrid.getLocationOfTile(nextTileNum);
			path.add(new LineSegment(currentPos, nextPos));
		}
		return path;
	}

	public static PVector velocityToTarget(PVector cLoc, PVector tLoc) {
		PVector velocity = PVector.sub(tLoc, cLoc);

		if (velocity.mag() < RADIUS_OF_SATISFACTION) {
			return new PVector(0, 0);
		}

		velocity.normalize();
		velocity.mult(MAX_SPEED);

		return velocity;
	}

	private static void startTimer() {
		foodTimer = System.currentTimeMillis();
	}

	private boolean checkTimer() {
		return System.currentTimeMillis() - foodTimer >= FOOD_INTERVAL;
	}

	private static void placeFood() {
		PVector emptyLocation = tileGrid.getRandomLocation();

		while (Area.getAreaFromLocation(emptyLocation).equals(Area.HALLWAY)) {
			emptyLocation = tileGrid.getRandomLocation();
		}

		food1 = new Food((int) emptyLocation.x, (int) emptyLocation.y, 5, 5);

		food1.setColorR((short) 0);
		food1.setColorG((short) 200);
		food1.setColorB((short) 0);
		food1.setObstacle(true);
		tileGrid.addTile(food1);

		emptyLocation = tileGrid.getRandomLocation();

		while (Area.getAreaFromLocation(emptyLocation).equals(Area.HALLWAY)) {
			emptyLocation = tileGrid.getRandomLocation();
		}

		food2 = new Food((int) emptyLocation.x, (int) emptyLocation.y, 5, 5);

		food2.setColorR((short) 0);
		food2.setColorG((short) 200);
		food2.setColorB((short) 0);
		food2.setObstacle(true);
		tileGrid.addTile(food2);

		foodItems = new ArrayList<>();
		foodItems.add(food1);
		foodItems.add(food2);

		determineAvailableFood();
	}

	public static void eatFood(Food f) {
		/*
		 * f.setColorR((short) 255); f.setColorG((short) 255);
		 * f.setColorB((short) 255); f.setObstacle(false);
		 */
		foodItems.remove(f);
		tileGrid.removeTile(f);
		f = null;

		ArrayList<Food> availableFood = determineAvailableFood();

		hero.setAvailableFoodItems(availableFood);
		monster.setAvailableFoodItems(availableFood);
		
		if (TWO_MONSTERS) {
			monster2.setAvailableFoodItems(availableFood);
		}

		if (availableFood.isEmpty() && foodTimer == 0) {
			startTimer();
		}
	}

	public static ArrayList<Food> determineAvailableFood() {
		ArrayList<Food> availableFood = new ArrayList<>();

		if (foodItems != null) {
			for (int i = 0; i < foodItems.size(); i++) {
				Food currentFood = foodItems.get(i);

				if (currentFood != null && currentFood.isAvailable()) {
					availableFood.add(currentFood);
				}
			}
		}

		hero.setAvailableFoodItems(availableFood);
		monster.setAvailableFoodItems(availableFood);

		return availableFood;
	}

	public static void monsterWins() {
		resetting = true;
	}

	public static void monsterDoneDancing() {
		resetGame();
	}

	private static void resetGame() {
		tileGrid = new TileGrid("tilegrid.txt");
		g = TileGraph.createGraph(tileGrid);
		((TileGraph) g).addCoordInfo(tileGrid);

		if (monster == null) {
			monster = new Character(tileGrid.getRandomLocation(),
					Math.PI / 2.0, CharacterBehavior.Type.MONSTER);
			//monster = new Character(new PVector(WIDTH / 2 - 20, HEIGHT / 2),
				//	Math.PI / 2.0, CharacterBehavior.Type.MONSTER);
		} else {
			monster.setPosition(tileGrid.getRandomLocation());
			((Character.MonsterBehavior) monster.getBehavior()).setNotHostile();
			((Character.HeroBehavior) hero.getBehavior())
					.setMonsterNotAThreat();
			monster.refillStamina();
			hero.refillStamina();
		}
		monster.setColor(255, 0, 0);
		//System.out.println("Moster's location: " + monster.getPosition());
		
		if (TWO_MONSTERS) {
			if (monster2 == null) {
				monster2 = new Character(tileGrid.getRandomLocation(),
						Math.PI / 2.0, CharacterBehavior.Type.MONSTER);
				//monster = new Character(new PVector(WIDTH / 2 - 20, HEIGHT / 2),
					//	Math.PI / 2.0, CharacterBehavior.Type.MONSTER);
			} else {
				monster2.setPosition(tileGrid.getRandomLocation());
				((Character.MonsterBehavior) monster2.getBehavior()).setNotHostile();
				monster2.refillStamina();
			}
			monster2.setColor(255, 0, 0);
		}

		if (hero == null) {
			hero = new Character(tileGrid.getRandomLocation(),
					Math.PI / 2.0, CharacterBehavior.Type.HERO);
		} else {
			hero.setPosition(tileGrid.getRandomLocation());
			((Character.MonsterBehavior) monster.getBehavior()).setNotHostile();
			((Character.HeroBehavior) hero.getBehavior())
					.setMonsterNotAThreat();
			monster.refillStamina();
			hero.refillStamina();
		}

		hero.setColor(0, 0, 0);
		((Character.HeroBehavior) hero.getBehavior()).setMonster(monster);

		((Character.MonsterBehavior) monster.getBehavior()).setHero(hero);
		
		if (TWO_MONSTERS) {
			((Character.MonsterBehavior) monster2.getBehavior()).setHero(hero);
			((Character.MonsterBehavior) monster2.getBehavior()).runDecisionTree();
		} else if (!RUN_MONSTER_BEHAVIOR_TREE) {
			((Character.MonsterBehavior) monster.getBehavior()).runDecisionTree();
		}

		breadcrumbs = new LinkedList<>();
		delay = 0;
		foodTimer = 0;
		showBreadcrumbs = false;
		showPath = false;
		resetting = false;
		placeFood();
	}

	private void prepareExitHandler() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				if (LOG_OUTPUT && writer != null) {
					try {
						writer.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

		}));

	}
	
	public static void writeLine() {
		if (LOG_OUTPUT && writer != null) {
			try {
				writer.write(((Character.MonsterBehavior) monster.getBehavior()).getGameState().getOutputString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
