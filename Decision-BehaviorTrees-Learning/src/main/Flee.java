package main;
import processing.core.PVector;


public class Flee {
	public static Steering getSteering(Character c, PVector target) {
		Steering s = new Steering();
		
		s.linear = PVector.sub(c.getPosition(), target);
		
		s.linear.normalize();
		s.linear = PVector.mult(s.linear, (float) c.getMaxAcceleration());
		s.angular = 0;
		
		return s;
	}
}
