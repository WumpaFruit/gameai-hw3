package main;
import java.util.ArrayList;
import java.util.Random;

import behaviortree.monster.MonsterBehaviorSelector;
import behaviortree.monster.MonsterSurviveAndKillRoutine;
import behaviortree.monster.WanderSequencer;
import behaviortree.monster.dance.AlignToOrientation;
import processing.core.PApplet;
import processing.core.PVector;

/**
 * A "boid" character on the screen.
 * @author Joseph Sankar
 *
 */
public class Character {
	private static final int DEFAULT_RADIUS = 5;
	private static final double BOID_DRAW_ANGLE = Math.PI / 4.0;
	private static final double DEFAULT_MAX_SPEED = 100f;
	private static final double DEFAULT_MAX_ROTATION = Math.PI * 2;
	private static final double DEFAULT_MAX_ACCELERATION = 2000;
	private static final float HERO_PERCEPTION_RADIUS = 100;
	private static final double HERO_PERCEPTION_ANGLE = Math.PI / 4.0;
	private static final float MONSTER_PERCEPTION_RADIUS = 50;
	private static final double MONSTER_PERCEPTION_ANGLE = Math.PI / 4.0;
	
	/** The maximum amount of stamina possible **/
	public static final float STAMINA_MAX = 100;
	
	/** How much stamina should decay per second **/
	private static final float STAMINA_MAX_DECAY_RATE = 10;
	
	private PVector position;
	private double orientation;
	private PVector velocity;
	private double rotation;
	private int radius;
	private float rColor;
	private float gColor;
	private float bColor;
	private PVector target;
	private LineSegmentPath currentPath;
	private float mass;
	private Steering steering;
	private double maxSpeed;
	private double maxRotation;
	private double maxAcceleration;
	private PVector perceptionWindowLeft, perceptionWindowRight;
	
	/** The area that character is in **/
	private Area area;
	
	/** The amount of stamina the character has **/
	private float stamina;
	
	/** The available food items in the environment **/
	private ArrayList<Food> availableFoodItems;
	
	/** What the character is doing **/
	private String state;
	
	private CharacterBehavior behavior;
	
	public Character(PVector pos, CharacterBehavior.Type t) {
		this(pos, 0, new PVector(0, 0), 0, t);
	}
	
	public Character(PVector pos, double orientation, CharacterBehavior.Type t) {
		this(pos, orientation, new PVector(0, 0), 0, t);
	}
	
	public Character(PVector position, double orientation,
			         PVector velocity, double rotation, CharacterBehavior.Type t) {
		this.position = position;
		this.orientation = orientation;
		this.velocity = velocity;
		this.rotation = rotation;
		this.radius = DEFAULT_RADIUS;
		rColor = 0;
		gColor = 0;
		bColor = 0;
		mass = 1;
		maxSpeed = DEFAULT_MAX_SPEED;
		maxRotation = DEFAULT_MAX_ROTATION;
		maxAcceleration = DEFAULT_MAX_ACCELERATION;
		steering = new Steering();
		state = "";
		
		if (behavior instanceof HeroBehavior) {
			perceptionWindowLeft = Vector.vectorFromRadians(orientation + HERO_PERCEPTION_ANGLE);
			perceptionWindowRight = Vector.vectorFromRadians(orientation - HERO_PERCEPTION_ANGLE);
			perceptionWindowLeft.mult(HERO_PERCEPTION_RADIUS);
			perceptionWindowRight.mult(HERO_PERCEPTION_RADIUS);
		} else {
			perceptionWindowLeft = Vector.vectorFromRadians(orientation + MONSTER_PERCEPTION_ANGLE);
			perceptionWindowRight = Vector.vectorFromRadians(orientation - MONSTER_PERCEPTION_ANGLE);
			perceptionWindowLeft.mult(MONSTER_PERCEPTION_RADIUS);
			perceptionWindowRight.mult(MONSTER_PERCEPTION_RADIUS);
		}
		
		perceptionWindowLeft.add(position);
		perceptionWindowRight.add(position);
		
		switch (t) {
		case NONE:
			behavior = new NoBehavior();
			break;
		case HERO:
			behavior = new HeroBehavior();
			break;
		case MONSTER:
			behavior = new MonsterBehavior();
			break;
		}
	}
	
	/**
	 * Draws the character.
	 * @param parent The applet where this character is being drawn
	 * @param breadcrumb Whether this character is a breadcrumb
	 */
	public void draw(PApplet parent, boolean breadcrumb) {
		PVector v1 = Vector.vectorFromRadians(orientation + BOID_DRAW_ANGLE);
		PVector v2 = Vector.vectorFromRadians(orientation - BOID_DRAW_ANGLE);
		PVector v4 = Vector.vectorFromRadians(orientation);
		v4.mult(radius * 2);
		PVector v3 = PVector.add(position, v4);
		//System.out.println(v3);
		v1.mult(radius);
		v2.mult(radius);
		v1 = PVector.add(position, v1);
		v2 = PVector.add(position, v2);
		
		if (behavior instanceof HeroBehavior) {
			perceptionWindowLeft = Vector.vectorFromRadians(orientation + HERO_PERCEPTION_ANGLE);
			perceptionWindowRight = Vector.vectorFromRadians(orientation - HERO_PERCEPTION_ANGLE);
			perceptionWindowLeft.mult(HERO_PERCEPTION_RADIUS);
			perceptionWindowRight.mult(HERO_PERCEPTION_RADIUS);
		} else {
			perceptionWindowLeft = Vector.vectorFromRadians(orientation + MONSTER_PERCEPTION_ANGLE);
			perceptionWindowRight = Vector.vectorFromRadians(orientation - MONSTER_PERCEPTION_ANGLE);
			perceptionWindowLeft.mult(MONSTER_PERCEPTION_RADIUS);
			perceptionWindowRight.mult(MONSTER_PERCEPTION_RADIUS);
		}
		
		perceptionWindowLeft.add(position);
		perceptionWindowRight.add(position);
		
		parent.stroke(rColor, gColor, bColor);
		if (!breadcrumb) {
			parent.fill(rColor, gColor, bColor);
			parent.triangle(v1.x, v1.y, v2.x, v2.y,
			        v3.x, v3.y);
			parent.ellipse(position.x, position.y, radius * 2, radius * 2);
			parent.noFill();
			parent.triangle(position.x, position.y, perceptionWindowLeft.x, perceptionWindowLeft.y,
					perceptionWindowRight.x, perceptionWindowRight.y);
		} else {
			parent.noFill();
			parent.line(v1.x, v1.y, v3.x, v3.y);
			parent.line(v2.x, v2.y, v3.x, v3.y);
			parent.ellipse(position.x, position.y, radius * 2, radius * 2);
		}
	}
	
	public void update(int time) {
		steering = new Steering();
		float timeInSeconds = time / 1000f;
		
		behavior.update(time);
		
		//Update velocity and rotation
		velocity = PVector.add(velocity, PVector.mult(steering.linear, timeInSeconds));
		rotation += steering.angular * timeInSeconds;	
		
		//Clip to max speed
		if (velocity.mag() > maxSpeed) {
			velocity.normalize();
			velocity = PVector.mult(velocity, (float) maxSpeed);
		}
		
		//Update position and orientation
		position = PVector.add(position, PVector.mult(velocity, timeInSeconds));
		//System.out.println("position is " + position);
		orientation += rotation * timeInSeconds;	
		
		if (orientation >= Math.PI * 2) {
			orientation -= Math.PI * 2;
		}
	}
	
	public void setColor(float r, float g, float b) {
		rColor = r;
		gColor = g;
		bColor = b;
	}

	public PVector getPosition() {
		return position;
	}

	public double getOrientation() {
		return orientation;
	}

	public PVector getVelocity() {
		return velocity;
	}

	public void setVelocity(PVector velocity) {
		this.velocity = velocity;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public PVector getTarget() {
		return target;
	}

	public void setTarget(PVector target) {
		this.target = target;
	}
	
	public CharacterBehavior getBehavior() {
		return behavior;
	}
	
	public float getMass() {
		return mass;
	}
	
	public void setMass(float newMass) {
		mass = newMass;
	}	
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public double getMaxRotation() {
		return maxRotation;
	}
	
	public class HeroBehavior extends CharacterBehavior {
		private static final long INVINCIBILITY_TIME = 5000;
		private Character monster;
		private Random rand;
		private LineSegmentPath wanderPath, fleeMonsterPath, userDefinedPath, closestFoodPath, safestFoodPath;
		private Food closestFood;
		private PVector userDefinedLoc;
		
		private boolean monsterAThreat;
		private boolean knowMonsterChasingMe;
		private long invincibilityTimer;
		private boolean invincible;
		
		public HeroBehavior() {
			mode = 1;
			rand = new Random();
			refillStamina();
			monsterAThreat = false;
			knowMonsterChasingMe = false;
			invincible = false;
		}

		@Override
		public void update(int time) {
			updateState(time);
			
			//TODO Change food paths when captured
			
			if (invincible && System.currentTimeMillis() - invincibilityTimer >= INVINCIBILITY_TIME) {
				invincible = false;
				((Character.MonsterBehavior)monster.getBehavior()).thaw();
			}
			
			if (!invincible && (monsterIsVisible() || monsterAThreat)) {	//Do I see the monster or know the monster is chasing me?
				state = "Saw monster!";
				if (knowMonsterChasingMe) { //Do I know the monster is chasing me?
					if (availableFoodItems.size() > 0 && stamina < 50) {
						fleeMonsterPath = null;
						if (closestFoodPath == null || foodTaken()) { //If there is no path, make one
							generateClosestFoodPath();
						} else {
							state = "Getting closest food while being chased";
							Steering closestFoodSteering = followClosestFoodPath();
							
							if (closestFoodSteering == null) { //If at food, refill stamina
								IndoorPathfinding.eatFood(closestFood);
								refillStamina();
								wanderPath = null; //Make previous wanderPath (if any) invalid
								closestFoodPath = null; //Make closestFoodPath invalid
								userDefinedPath = null;
							} else {
								steering.add(closestFoodSteering);
							}
						}
					} else { //Flee from the monster
						state = "Fleeing from monster";
						
						if (fleeMonsterPath == null) {
							generateFleeMonsterPath();
						} else {
							Steering fleeMonsterSteering = fleeMonster();
							if (fleeMonsterSteering == null) { //If we have arrived, generate a new path
								generateFleeMonsterPath();
							} else {
								steering.add(fleeMonsterSteering);
							}
						}
						
						//TODO: Add collision avoidance
						
						//If we are fleeing the monster and it gives up, it is no longer a threat
						if (!((Character.MonsterBehavior)monster.getBehavior()).isHostile()) {
							monsterAThreat = false;
							knowMonsterChasingMe = false;
						}
					}
				} else {
					state = "Time for a sneak attack";
					
					Steering sneakAttackSteering = moveToSneakAttack();
					
					if (sneakAttackSteering == null) {
						state = "Performing visceral attack";
						
						((Character.MonsterBehavior)monster.getBehavior()).freeze();
						((Character.MonsterBehavior)monster.getBehavior()).getGameState().character.stopAngularMovement();
						monsterAThreat = false;
						invincible = true;
						invincibilityTimer = System.currentTimeMillis();
					} else {
						steering.add(sneakAttackSteering);
					}
				}
			} else { //I do not perceive the monster as a threat
				if (availableFoodItems.size() > 0 && stamina < 25) { //Should I get food?
					if (closestFoodPath == null || foodTaken()) { //If there is no path, make one
						generateClosestFoodPath();
					} else {
						state = "Getting closest food";
						Steering closestFoodSteering = followClosestFoodPath();
						
						if (closestFoodSteering == null || PVector.dist(position, new PVector(closestFood.getX(), closestFood.getY())) < 5) { //If at food, refill stamina
							IndoorPathfinding.eatFood(closestFood);
							refillStamina();
							wanderPath = null; //Make previous wanderPath (if any) invalid
							closestFoodPath = null; //Make closestFoodPath invalid
							userDefinedPath = null;
						} else {
							steering.add(closestFoodSteering);
						}
					}
				} else if (userDefinedLoc != null) {	//Did the user click somewhere?
					if (userDefinedPath == null) {
						generateUserDefinedPath();
						wanderPath = null;
					} else {
						state = "Following user click";
						Steering userDefinedSteering = followUserClick();
						
						if (userDefinedSteering == null) {
							userDefinedPath = null;
							userDefinedLoc = null;
							closestFoodPath = null;
							IndoorPathfinding.arrivedAtUserDefinedPath();
						} else {
							steering.add(userDefinedSteering);
						}
					}
				} else if (wanderPath == null) { //If there is no path to wander, generate one
					generateWanderRoomPath();
				} else {
					state = "Wandering";
					Steering wanderRoomSteering = wanderRooms();
					if (wanderRoomSteering == null) { //If we have arrived, generate a new path
						generateWanderRoomPath();
					} else {
						steering.add(wanderRoomSteering);
					}
				}
			}

			Steering lookSteering = LookWhereYoureGoing.getSteering(Character.this);
			if (lookSteering != null) {
				steering.add(lookSteering);
			}				
		}

		private boolean foodTaken() {
			return !availableFoodItems.contains(closestFood);
		}

		private Steering moveToSneakAttack() {
			Steering steering = Seek.getSteering(Character.this, monster.getPosition());
			if (PVector.dist(position, monster.position) <= 1) {
				return null;
			}
			return steering;
		}

		private boolean monsterIsVisible() {
			if (pointInTriangle(monster.getPosition(), position, perceptionWindowLeft, perceptionWindowRight)) {
				monsterAThreat = true;
				if (((MonsterBehavior)monster.getBehavior()).isHostile()) {
					knowMonsterChasingMe = true;
				}
				return true;
			}
			
			return false;
		}
		
		private void generateUserDefinedPath() {
			userDefinedPath = IndoorPathfinding.pathfindAStar(position, userDefinedLoc);
			
		}

		private Steering followUserClick() {
			return FollowPath.getSteering(Character.this, userDefinedPath);
		}

		private void generateClosestFoodPath() {
			Food closestFood = null;
			float closestFoodDistance = Float.MAX_VALUE;
			for (int i = 0; i < availableFoodItems.size(); i++) {			
				Food currentFood = availableFoodItems.get(i);
				float currentFoodDistance = Math.abs(position.x - currentFood.getX()) + Math.abs(position.y - currentFood.getY());
				if (currentFoodDistance < closestFoodDistance) {
					closestFood = currentFood;
					closestFoodDistance = currentFoodDistance;
				}
			}
			
			this.closestFood = closestFood;
			closestFoodPath = IndoorPathfinding.pathfindAStar(position, new PVector(closestFood.getX(), closestFood.getY()));
		}
		
		private Steering followClosestFoodPath() {
			return FollowPath.getSteering(Character.this, closestFoodPath);
		}
		
		private void generateFleeMonsterPath() {
			int currentArea = area.ordinal();
			int room7Num = Area.ROOM7.ordinal();
			int room8Num = Area.ROOM8.ordinal();
			int randAreaNum = rand.nextInt(18) + 1;
			
			while (randAreaNum == currentArea || randAreaNum == room7Num ||
					randAreaNum == room8Num) {
				randAreaNum = rand.nextInt(18) + 1;
			}
			
			PVector target = Area.getRandomLocationInArea(Area.values()[randAreaNum]);
			
			fleeMonsterPath = IndoorPathfinding.pathfindAStar(position, target); 			
		}
		
		private Steering fleeMonster() {
			return FollowPath.getSteering(Character.this, fleeMonsterPath);
		}
		
		private void generateWanderRoomPath() {
			int currentArea = area.ordinal();
			int randAreaNum = rand.nextInt(18) + 1;
			
			while (randAreaNum == currentArea) {
				randAreaNum = rand.nextInt(18) + 1;
			}
			
			PVector target = Area.getRandomLocationInArea(Area.values()[randAreaNum]);
			
			wanderPath = IndoorPathfinding.pathfindAStar(position, target);
		}
		
		private Steering wanderRooms() {
			return FollowPath.getSteering(Character.this, wanderPath);
			
			//gameState.getRandomLocationInArea(area)
		}
		
		private void updateState(int time) {
			float timeInSeconds = time / 1000f;
			
			if (velocity.mag() >= 0.01f && stamina > 0) {
				if (monsterAThreat) {
					stamina -= timeInSeconds * STAMINA_MAX_DECAY_RATE;
				} else {
					stamina -= timeInSeconds * (STAMINA_MAX_DECAY_RATE / 2f);
				}
				if (stamina < 0) {
					stamina = 0;
				}
			}
			
			area = Area.getAreaFromLocation(position);
			
			if (stamina == 0) {
				maxSpeed = DEFAULT_MAX_SPEED / 2f;
			} else if (area == Area.HALLWAY) {
				maxSpeed = DEFAULT_MAX_SPEED * 1.5f;
			} else if (monsterAThreat){
				maxSpeed = DEFAULT_MAX_SPEED;
			} else {
				maxSpeed = DEFAULT_MAX_SPEED * .75f;
			}
			
			state = "Undefined";
		}
		
		public void setMonster(Character monster) {
			this.monster = monster;
		}
		
		public void setUserDefinedLocation(PVector loc) {
			userDefinedLoc = loc;
		}
		
		public void setMonsterNotAThreat() {
			monsterAThreat = false;
		}
	}
	
	public class MonsterBehavior extends CharacterBehavior {
		/** Time until monster is not hostile **/
		private static final float COOLDOWN_TIME = 1000;
		
		private MonsterBehaviorSelector tree;
		
		private GameState gameState;
		private boolean frozen;
		private long speedBias;
		private boolean runDecisionTree;
		
		public MonsterBehavior() {
			refillStamina();
			gameState = new GameState();
			gameState.rand = new Random();
			tree = new MonsterBehaviorSelector();
			//TODO: Remove next line!!!
			frozen = false;
			runDecisionTree = false;
			gameState.monsterCelebrating = false;
		}

		@Override
		public void update(int time) {
			updateState(time);
			
			if (!frozen) {
				if (!runDecisionTree) {
					tree.perform(gameState);
				} else {
					lookForHero();
					heroLost();
					executeDecisionTree();
				}
			} else {
				velocity = new PVector(0, 0);
			}
			
			steering = gameState.steering;
			state = gameState.status;
			
			Steering lookSteering = LookWhereYoureGoing.getSteering(Character.this);
			if (lookSteering != null) {
				steering.add(lookSteering);
			}		
		}
		
		private void executeDecisionTree() {
			if (gameState.wanderPath != null) {
				gameState.pathFollowWanderSteering = FollowPath.getSteering(gameState.character, gameState.wanderPath);
				if (gameState.pathFollowWanderSteering != null) {
					pathfollowWanderTarget();
				} else {
					arrivedAtWanderTarget();
				}
			} else {
				if (gameState.closestFoodPath != null) {
					gameState.pathFollowToFoodSteering = FollowPath.getSteering(gameState.character, gameState.closestFoodPath);
					if (gameState.pathFollowToFoodSteering != null) {
						if (gameState.foodAvailable()) {
							pathfollowToFood();
						} else {
							if (gameState.wanderTarget != null) {
								generateWanderPath();
							} else {
								generateWanderTarget();
							}
						}
					} else {
						eatFood();
					}
				} else {
					if (gameState.dancePauseCompleted()) {
						seekHero();
						if (gameState.seekHeroSteering == null) {
							if (gameState.hostile) {
								if (gameState.dancePauseTimerSet()) {
									moveToNextDancePosition();
								} else {
									if (gameState.danceTimerSet()) {
										setPauseTimer();
									} else {
										if (gameState.monsterCelebrating) {
											generateAlignOrientationSteering();
											
											if (gameState.alignOrientationSteering != null) {
												startToDance();
											} else {
												generateOrderOfOrientations();
											}
										} else {
											eatHero();
										}
									}
								}
							} else {
								if (gameState.wanderTarget != null) {
									generateWanderPath();
								} else {
									if (gameState.staminaLow()) {
										if (gameState.closestFoodTarget != null) {
											generateFoodPath();
										} else {
											if (gameState.foodAvailable()) {
												generateFoodTarget();
											} else {
												generateWanderTarget();
											}
										}
									} else {
										generateWanderTarget();
									}
								}
							}
						} 
					} else {
						if (gameState.danceOrientationsGenerated) {
							if (gameState.danceCompleted()) {
								doneDancing();
							} else {
								moveTowardDanceOrientation();
							}
						} else {
							arrivedAtTargetOrientation();
						}
					}
				}
			}
		}
		
		private void eatHero() {
			gameState.status = "Eating hero...delicious";
			IndoorPathfinding.monsterWins();
			velocity = new PVector(0, 0);
			gameState.monsterCelebrating = true;
		}
		
		private void seekHero() {
			
			if (gameState.hostile) {
				gameState.seekHeroSteering = Seek.getSteering(Character.this,
						gameState.hero.getPosition());
	
				if (gameState.seekHeroSteering != null) {
					gameState.steering.add(gameState.seekHeroSteering);
				}
				gameState.status = "In pursuit";
			}
		}
		
		private void generateWanderPath() {
			gameState.wanderPath = IndoorPathfinding.pathfindAStar(position, gameState.wanderTarget);
		}
		
		private void generateWanderTarget() {
			int currentArea = gameState.area.ordinal();
			int randAreaNum = gameState.rand.nextInt(18) + 1;
			
			while (randAreaNum == currentArea) {
				randAreaNum = gameState.rand.nextInt(18) + 1;
			}
			
			gameState.wanderTarget = Area.getRandomLocationInArea(Area.values()[randAreaNum]);
			gameState.wanderPath = null;
			
			gameState.status = "Generating wander target";
		}
		
		private void pathfollowWanderTarget() {
			gameState.steering.add(gameState.pathFollowWanderSteering);
			
			gameState.status = "On the prowl";

			IndoorPathfinding.writeLine();
		}
		
		private void arrivedAtWanderTarget() {
			gameState.status = "Arrived at wander target";
	
			gameState.wanderTarget = null;
			gameState.wanderPath = null;
			gameState.closestFood = null;
			gameState.closestFoodTarget = null;
			gameState.closestFoodPath = null;
		}
		
		private void generateFoodTarget() {
			gameState.status = "Generating food target";
			
			Food closestFood = null;
			float closestFoodDistance = Float.MAX_VALUE;
			for (int i = 0; i < gameState.availableFoodItems.size(); i++) {			
				Food currentFood = gameState.availableFoodItems.get(i);
				float currentFoodDistance = Math.abs(position.x - currentFood.getX()) + Math.abs(position.y - currentFood.getY());
				if (currentFoodDistance < closestFoodDistance) {
					closestFood = currentFood;
					closestFoodDistance = currentFoodDistance;
				}
			}
			
			gameState.closestFood = closestFood;
			gameState.closestFoodTarget = new PVector(closestFood.getX(), closestFood.getY());
			gameState.closestFoodPath = null;
		}
		
		private void generateFoodPath() {
			gameState.status = "Generating food path";
			
			gameState.closestFoodPath = IndoorPathfinding.pathfindAStar(position, new PVector(gameState.closestFood.getX(), gameState.closestFood.getY()));
		}
		
		private void pathfollowToFood() {
			gameState.steering.add(gameState.pathFollowToFoodSteering);
			gameState.status = "Heading toward food";
		}
		
		private void eatFood() {
			gameState.status = "Eating food";
			
			IndoorPathfinding.eatFood(gameState.closestFood);
			refillStamina();
			gameState.wanderTarget = null;
			gameState.wanderPath = null;
			gameState.closestFood = null;
			gameState.closestFoodPath = null;
		}
		
		private void generateOrderOfOrientations() {			
			gameState.status = "Generating dance orientations";
			
			gameState.danceOrientations = new ArrayList<>();
			
			gameState.danceOrientations.add(orientation + (Math.PI / 2));
			gameState.danceOrientations.add(orientation + Math.PI);
			gameState.danceOrientations.add(orientation + (3 * Math.PI / 2));
			gameState.danceOrientations.add(orientation);

			
			gameState.danceOrientationsGenerated = true;
		}
		
		private void doneDancing() {
			gameState.status = "Done dancing";

			gameState.monsterCelebrating = false;
			stopAngularMovement();
			IndoorPathfinding.monsterDoneDancing();	
			gameState.danceOrientationsGenerated = false;
			gameState.danceTimer = 0L;
			gameState.hostile = false;		
		}
		
		private void moveToNextDancePosition() {
			gameState.status = "Moving to next dancing position";

			gameState.dancePauseTimer = 0L;
			gameState.run++;
			
			if (gameState.run >= gameState.danceOrientations.size()) {
				gameState.run = 0;
			}
		}
		
		private void setPauseTimer() {
			gameState.status = "Setting pause timer";
			
			gameState.dancePauseTimer = System.currentTimeMillis();
		}
		
		private void startToDance() {
			gameState.status = "Starting to dance";
			
			gameState.danceTimer = System.currentTimeMillis();
		}
		
		private void moveTowardDanceOrientation() {
			gameState.steering.add(gameState.alignOrientationSteering);
		}
		
		private void generateAlignOrientationSteering() {
			if (gameState.danceOrientations != null) {
				gameState.alignOrientationSteering = Align.getSteering(Character.this, gameState.danceOrientations.get(gameState.run));
			}
		}
		
		private void arrivedAtTargetOrientation() {
			stopAngularMovement();
			gameState.status = "Arrived at target orientation";
		}
		
		public void sneakAttack() {
			frozen = true;
			gameState.hostile = false;
		}
		
		private void heroLost() {
			if (System.currentTimeMillis() - gameState.chaseTimer >= COOLDOWN_TIME) {
				gameState.hostile = false;
			}
		}
		
		private void lookForHero() {
			if (pointInTriangle(gameState.hero.getPosition(), position, gameState.perceptionWindowLeft, gameState.perceptionWindowRight)) {
				gameState.hostile = true;
				gameState.wanderTarget = null;
				gameState.wanderPath = null;
				gameState.closestFoodTarget = null;
				gameState.closestFoodPath = null;
				gameState.chaseTimer = System.currentTimeMillis();
			}
		}
		
		private void updateState(int time) {
			float timeInSeconds = time / 1000f;
			
			if (velocity.mag() >= 0.01f && stamina > 0) {
				stamina -= timeInSeconds * STAMINA_MAX_DECAY_RATE;
				
				if (stamina < 0) {
					stamina = 0;
				}
			}
			
			gameState.area = Area.getAreaFromLocation(position);
			
			if (stamina == 0) {
				maxSpeed = (DEFAULT_MAX_SPEED / 2f) + speedBias;
			} else {
				//maxSpeed = DEFAULT_MAX_SPEED * .75f + speedBias;
				maxSpeed = DEFAULT_MAX_SPEED + speedBias;
			}
			
			gameState.character = Character.this;
			gameState.steering = new Steering();
			gameState.availableFoodItems = availableFoodItems;
			gameState.perceptionWindowLeft = perceptionWindowLeft;
			gameState.perceptionWindowRight = perceptionWindowRight;
		}
		
		public void setHero(Character hero) {
			gameState.hero = hero;
		}
		
		public boolean isHostile() {
			return gameState.hostile;
		}
		
		public void setNotHostile() {
			gameState.hostile = false;
		}
		
		public void freeze() {
			frozen = true;
		}
		
		public void thaw() {
			frozen = false;
			speedBias += 10;
		}
		
		public GameState getGameState() {
			return gameState;
		}
		
		public void runDecisionTree() {
			runDecisionTree = true;
		}
	}
	
	class NoBehavior extends CharacterBehavior{

		@Override
		public void update(int time) { }
		
	}
	
	//From http://www.blackpawn.com/texts/pointinpoly/
	private boolean pointInTriangle(PVector p, PVector a, PVector b, PVector c) {
		return sameSide(p, a, b, c) && sameSide(p, b, a, c) && sameSide(p, c, a, b);
	}
	
	//From http://www.blackpawn.com/texts/pointinpoly/
	private boolean sameSide(PVector p1, PVector p2, PVector a, PVector b) {
		//PVector cp1 = null, cp2 = null;
		PVector cp1 = PVector.cross(PVector.sub(b, a), PVector.sub(p1, a), null);
		PVector cp2 = PVector.cross(PVector.sub(b, a), PVector.sub(p2, a), null);
		
		return PVector.dot(cp1, cp2) >= 0;
	}
	
	public void setPath(LineSegmentPath p) {
		currentPath = p;
	}
	
	public LineSegmentPath getPath() {
		return currentPath;
	}
	
	public double getMaxAcceleration() {
		return maxAcceleration;
	}
	
	public void setPosition(PVector newPos) {
		position = newPos;
	}
	
	public Area getArea() {
		return area;
	}
	
	public float getStamina() {
		return stamina;
	}
	
	public void refillStamina() {
		stamina = STAMINA_MAX;
	}
	
	public void depleteStamina() {
		stamina = 0;
	}
	
	public void setAvailableFoodItems(ArrayList<Food> food) {
		availableFoodItems = food;
	}
	
	public String getState() {
		return state;
	}
	
	public void stopAngularMovement() {
		rotation = 0;
		steering.angular = 0;
	}
}