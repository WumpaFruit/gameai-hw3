package decisiontreelearning;

import java.util.HashSet;
import java.util.Set;


public class Attribute {
	private String name;
	private Set<Value> values;
	
	public Attribute(String name) {
		this.name = name;
		values = new HashSet<>();
	}
	
	public void addValue(Value v) {
		values.add(v);
	}
	
	public String getName() {
		return name;
	}
	
	public Set<Value> getValues() {
		return values;
	}
	
	public String toString() {
		String s = "";
		Value[] valueArray = values.toArray(new Value[values.size()]);
		
		
		if (valueArray.length > 0) {
			s += name + " = {";
			
			for (int i = 0; i < valueArray.length; i++) {
				if (i < valueArray.length - 1) {
					s += valueArray[i] + ", ";
				} else {
					s += valueArray[i];
				}
			}
			
			s += "}";
		} else {
			s += name;
		}
		
		return s;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Attribute other = (Attribute) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
