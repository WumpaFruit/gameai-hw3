package decisiontreelearning;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DecisionTreeLearningRunner {
	public static final String FILENAME = "output.txt";
	private static ArrayList<Attribute> attributes;
	private static ArrayList<Run> runs;
	private static Action action;
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		attributes = new ArrayList<>();
		runs = new ArrayList<>();
		action = new Action("action");
		
		Scanner input = null;
		
		try {
			input = new Scanner(new File(FILENAME));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//For the first run, determine all possible values of all attributes and actions
		if (input != null) {
			String firstLine = input.nextLine();
			Scanner firstLineScan = new Scanner(firstLine);
			firstLineScan.useDelimiter(",");
			while (firstLineScan.hasNext()) {
				String str = firstLineScan.next();
				if (!str.equals("action")) {
					attributes.add(new Attribute(str));
				}
			}
			
			firstLineScan.close();
			
			while(input.hasNextLine()) {
				String line = input.nextLine();
				Scanner lineScan = new Scanner(line);
				lineScan.useDelimiter(",");
				
				for (int i = 0; i < attributes.size(); i++) {
					String value = lineScan.next();
					attributes.get(i).addValue((new Value(value)));
				}
				
				action.addValue(new Value(lineScan.next()));
				
				lineScan.close();
			}
			input.close();
			
			//System.out.println("Attributes: " + attributes);
			//System.out.println(action);
		}
			
		
		//Read through file again, now focusing on the runs
		try {
			input = new Scanner(new File(FILENAME));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (input != null) {
			input.nextLine();
			
			while (input.hasNextLine()) {
				String line = input.nextLine();
				Scanner lineScan = new Scanner(line);
				lineScan.useDelimiter(",");
				ArrayList<Value> runValues = new ArrayList<>();
				Value actionValue = null;
				for (int i = 0; i < attributes.size(); i++) {
					runValues.add(new Value(lineScan.next()));
				}
				actionValue = new Value(lineScan.next());
				runs.add(new Run((ArrayList<Attribute>)attributes.clone(), runValues, action, actionValue));
				
				lineScan.close();
			}
			
			for (int i = 0; i < runs.size(); i++) {
				////System.out.println(runs.get(i));
			}
		}
		
		heart(0, runs, attributes);
	}
	
	private static void heart(int level, ArrayList<Run> filteredRuns, ArrayList<Attribute> filteredAttributes) {
		boolean oneAction = true;
		
		if (filteredRuns.isEmpty()) {
			return;
		}
		
		Set<Value> actionValues = new HashSet<Value>();
		for (Run filteredRun: filteredRuns) {
			actionValues.add(filteredRun.getActionValue());
			if (actionValues.size() > 1) {	//Is there more than one possible action?
				oneAction = false;
				break;
			}
		}
		
		if (oneAction) {
			//Add a leaf with that action and exit
			//System.out.println("Leaf with action " + filteredRuns.get(0).getActionValue());
			printTabs(level);
			System.out.println(filteredRuns.get(0).getActionValue());
			return;
		}
		
		if (filteredAttributes.size() == 0) {
			//There are no more attributes, use probabilities
			//System.out.println("Assigning probabilities");
			HashMap<Value, Integer> actionValueCount = new HashMap<>();
			
			for (Run r: filteredRuns) {
				if (actionValueCount.containsKey(r.getActionValue())) {
					actionValueCount.put(r.getActionValue(), actionValueCount.get(r.getActionValue()) + 1);
				} else {
					actionValueCount.put(r.getActionValue(), 1);
				}
			}
			
			int totalActions = 0;
			
			for (Integer integer: actionValueCount.values()) {
				totalActions += integer;
			}
			
			printTabs(level);
			System.out.print("Probabilities: [");
			for (Value v: actionValueCount.keySet()) {
				System.out.print(v + " = " + (double)actionValueCount.get(v) / totalActions + ", ");
			}
			System.out.println("]");
			return;
		}
		
		Attribute lowestAttribute = null;
		
		if (filteredAttributes.size() == 1) {
			//If there is only one attribute, split on it
			//without calculating (potentially costly) information gain
			lowestAttribute = filteredAttributes.get(0);
			//System.out.println("Performing recursive call on one attribute: " + lowestAttribute.getName());
			ArrayList<Attribute> newFilteredAttributes = new ArrayList<>(filteredAttributes);
			newFilteredAttributes.remove(lowestAttribute);
			for (Value v: lowestAttribute.getValues()) {
				ArrayList<Run> newFilteredRuns = split(filteredRuns, lowestAttribute, v);
				//System.out.println("Filtered runs on " + lowestAttribute.getName() + " = " + v + ": " + newFilteredRuns);
				
				if (newFilteredRuns.size() > 0) {
					printTabs(level);
					System.out.println(lowestAttribute.getName() + " = " + v);
					heart(level + 1, newFilteredRuns, newFilteredAttributes);
				} else {
					//System.out.println("No leaf because there are no runs");
				}
			}
			return;
		}
		
		//We need to chose the attribute with the highest information gain and split on it
		
		Set<Value> filteredActionValues = new HashSet<>();
		
		for (Run r: filteredRuns) {
			filteredActionValues.add(r.getActionValue());
		}
		
		double entropy = entropy(filteredRuns, filteredActionValues);
		double lowestEntropy2 = Double.MAX_VALUE;
		Attribute splitAttribute = null;
		
		//Find the attribute that minimizes entropy2
		for (int i = 0; i < filteredAttributes.size(); i++) {
			double entropy2 = 0.0;
			
			for (Value value: filteredAttributes.get(i).getValues()) {
				entropy2 += entropyGivenAttributeValue(filteredRuns, filteredAttributes.get(i), value, filteredActionValues);
			}
			
			if (entropy2 < lowestEntropy2) {
				lowestEntropy2 = entropy2;
				splitAttribute = filteredAttributes.get(i);
			}
		}
		
		//System.out.println("Splitting on " + splitAttribute.getName());
		
		ArrayList<Attribute> newFilteredAttributes = new ArrayList<>(filteredAttributes);
		newFilteredAttributes.remove(splitAttribute);
		for (Value v: splitAttribute.getValues()) {
			ArrayList<Run> newFilteredRuns = split(filteredRuns, splitAttribute, v);
			//System.out.println("Filtered runs on " + splitAttribute.getName() + " = " + v + ": " + newFilteredRuns);
			printTabs(level);
			System.out.println(splitAttribute.getName() + " = " + v);
			heart(level + 1, newFilteredRuns, newFilteredAttributes);
		}
		////System.out.println(filteredAttributes);
	}
	
	private static ArrayList<Run> split(ArrayList<Run> filteredRuns,
			Attribute splitAttribute, Value v) {
		ArrayList<Run> splitRuns = new ArrayList<>();
		
		for (Run r: filteredRuns) {
			if (r.attributeValuePresent(splitAttribute, v)) {
				splitRuns.add(r);
			}
		}
		
		return splitRuns;
	}

	private static double entropy(ArrayList<Run> filteredRuns, Set<Value> filteredActionValues) {
		double entropy = 0.0;
		ArrayList<Integer> actionValueCount = new ArrayList<>(filteredActionValues.size());

		for (int i = 0; i < filteredActionValues.size(); i++) {
			actionValueCount.add(new Integer(0));
		}
		
		Value[] actionValueArray = filteredActionValues.toArray(new Value[filteredActionValues.size()]);
		
		for (int i = 0; i < actionValueArray.length; i++) {
			Value actionValue = actionValueArray[i];
			for (int j = 0; j < filteredRuns.size(); j++) {
				if (filteredRuns.get(j).actionValuePresent(actionValue)) {
					actionValueCount.set(i, actionValueCount.get(i) + 1);
				}
			}
			double fraction =  (double)actionValueCount.get(i) / filteredRuns.size();

			entropy -= fraction * (Math.log(fraction) / Math.log(2));
		}
		
		////System.out.println(actionValueCount + " " + filteredRuns.size() + " " + entropy);
		
		return entropy;
	}
	
	private static double entropyGivenAttributeValue(ArrayList<Run> filteredRuns, Attribute attribute, Value attributeValue, Set<Value> filteredActionValues) {
		double entropy = 0.0;
		
		////System.out.println("Running entropyGivenAttributeValue with " + attribute.getName() + " = " + attributeValue);
		
		ArrayList<Run> runsWithAttributeValue = new ArrayList<>();
		
		for (int i = 0; i < filteredRuns.size(); i++) {
			if (filteredRuns.get(i).attributeValuePresent(attribute, attributeValue)) {
				runsWithAttributeValue.add(filteredRuns.get(i));
			}
		}
		
		ArrayList<Integer> actionValueCount = new ArrayList<>(filteredActionValues.size());

		for (int i = 0; i < filteredActionValues.size(); i++) {
			actionValueCount.add(new Integer(0));
		}
		
		Value[] actionValueArray = filteredActionValues.toArray(new Value[filteredActionValues.size()]);
		
		for (int i = 0; i < actionValueArray.length; i++) {
			Value actionValue = actionValueArray[i];
			for (int j = 0; j < runsWithAttributeValue.size(); j++) {
				if (runsWithAttributeValue.get(j).actionValuePresent(actionValue)) {
					actionValueCount.set(i, actionValueCount.get(i) + 1);
				}
			}
			double fraction =  (double)actionValueCount.get(i) / runsWithAttributeValue.size();
			//System.out.print(fraction + " = " + actionValueCount.get(i) + " / " + runsWithAttributeValue.size() + ", ");
			
			if (fraction > 0) {
				entropy -= fraction * (Math.log(fraction) / Math.log(2));
			} else {
				////System.out.println("Not modifying entropy");
			}
		}
		////System.out.println();
		entropy *= (double)runsWithAttributeValue.size() / filteredRuns.size();
		////System.out.println(actionValueCount + " " + runsWithAttributeValue.size() + " " + entropy);
		
		if (Double.isNaN(entropy)) {
			return 0.0;
		}
		return entropy;
	}
	
	private static void printTabs(int n) {
		for (int i = 0; i < n; i++) {
			System.out.print("\t");
		}
	}
}
