package decisiontreelearning;

import java.util.ArrayList;

public class Run {
	private ArrayList<Attribute> attributes;
	private ArrayList<Value> values;
	private Action action;
	private Value actionValue;
	
	public Run(ArrayList<Attribute> attributes, ArrayList<Value> values, Action action,
				Value actionValue) {
		this.attributes = attributes;
		this.values = values;
		this.action = action;
		this.actionValue = actionValue;
	}
	
	public boolean actionValuePresent(Value actionValue) {
		return this.actionValue.equals(actionValue);
	}
	
	public boolean attributeValuePresent(Attribute desiredAttribute, Value desiredValue) {
		for (int i = 0; i < attributes.size(); i++) {
			Attribute attribute = attributes.get(i);
			if (attribute.equals(desiredAttribute)) {
				return values.get(i).equals(desiredValue);
			}
		}
		
		throw new IllegalArgumentException("Wrong attribute/value combination: " + desiredAttribute.getName() + " " + desiredValue + "\n" + this);
	}
	
	public Value getActionValue() {
		return actionValue;
	}
	
	public String toString() {
		String s = "[";
		
		for (int i = 0; i < attributes.size(); i++) {
				s += attributes.get(i).getName() + " = " + values.get(i) + ", ";
		}
		
		s += action.getName() + " = " + actionValue + "]";
		
		return s;
	}
}