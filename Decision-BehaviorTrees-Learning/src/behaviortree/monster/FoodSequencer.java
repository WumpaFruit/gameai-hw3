package behaviortree.monster;

import behaviortree.core.Sequencer;
import behaviortree.monster.food.AmIHungryAndIsThereFood;
import behaviortree.monster.food.FoodPathSelector;
import behaviortree.monster.food.FoodTargetSelector;
import behaviortree.monster.food.PathfollowToFoodSelector;

public class FoodSequencer extends Sequencer {
	public FoodSequencer() {
		subTasks.add(new AmIHungryAndIsThereFood());
		subTasks.add(new FoodTargetSelector());
		subTasks.add(new FoodPathSelector());
		subTasks.add(new PathfollowToFoodSelector());
	}
}
