package behaviortree.monster.food;

import processing.core.PVector;
import main.Food;
import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class GenerateFoodTarget extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Generating food target";
		IndoorPathfinding.writeLine();
		
		Food closestFood = null;
		float closestFoodDistance = Float.MAX_VALUE;
		for (int i = 0; i < state.availableFoodItems.size(); i++) {			
			Food currentFood = state.availableFoodItems.get(i);
			float currentFoodDistance = Math.abs(state.character.getPosition().x - currentFood.getX()) + Math.abs(state.character.getPosition().y - currentFood.getY());
			if (currentFoodDistance < closestFoodDistance) {
				closestFood = currentFood;
				closestFoodDistance = currentFoodDistance;
			}
		}
		
		state.closestFood = closestFood;
		state.closestFoodTarget = new PVector(closestFood.getX(), closestFood.getY());
		state.closestFoodPath = null;
			
		return true;
	}

}
