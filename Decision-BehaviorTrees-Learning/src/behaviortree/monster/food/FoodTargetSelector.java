package behaviortree.monster.food;

import behaviortree.core.Selector;

public class FoodTargetSelector extends Selector {
	public FoodTargetSelector() {
		subTasks.add(new DoesFoodTargetExist());
		subTasks.add(new GenerateFoodTarget());
	}
}
