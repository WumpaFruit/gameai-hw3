package behaviortree.monster.food;

import main.GameState;
import behaviortree.core.LeafTask;

public class AmIHungryAndIsThereFood extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		return state.character.getStamina() <= GameState.MONSTER_LOW_STAMINA_LEVEL && state.availableFoodItems.size() > 0;
	}

}
