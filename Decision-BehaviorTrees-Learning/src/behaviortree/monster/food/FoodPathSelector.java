package behaviortree.monster.food;

import behaviortree.core.Selector;

public class FoodPathSelector extends Selector {
	public FoodPathSelector() {
		subTasks.add(new DoesFoodPathExist());
		subTasks.add(new GenerateFoodPath());
	}
}
