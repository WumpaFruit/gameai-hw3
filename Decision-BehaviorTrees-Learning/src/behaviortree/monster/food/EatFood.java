package behaviortree.monster.food;

import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class EatFood extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Eating food";
		IndoorPathfinding.writeLine();
		
		IndoorPathfinding.eatFood(state.closestFood);
		state.character.refillStamina();
		state.wanderTarget = null;
		state.wanderPath = null;
		state.closestFood = null;
		state.closestFoodPath = null;
		return true;
	}

}
