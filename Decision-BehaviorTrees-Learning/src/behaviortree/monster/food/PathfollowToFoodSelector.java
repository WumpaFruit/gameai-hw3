package behaviortree.monster.food;

import behaviortree.core.Selector;

public class PathfollowToFoodSelector extends Selector {
	public PathfollowToFoodSelector() {
		subTasks.add(new PathfollowToFood());
		subTasks.add(new EatFood());
	}
}
