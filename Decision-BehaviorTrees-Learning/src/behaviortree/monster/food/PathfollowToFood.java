package behaviortree.monster.food;

import main.FollowPath;
import main.GameState;
import main.IndoorPathfinding;
import main.Steering;
import behaviortree.core.LeafTask;

public class PathfollowToFood extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Trying to head toward food";
		IndoorPathfinding.writeLine();
		
		state.pathFollowToFoodSteering = FollowPath.getSteering(state.character, state.closestFoodPath);
		
		if (state.pathFollowToFoodSteering != null) {
			state.steering.add(state.pathFollowToFoodSteering);
			state.status = "Heading toward food";
			IndoorPathfinding.writeLine();
			return true;
		} 
		
		return false;
	}

}
