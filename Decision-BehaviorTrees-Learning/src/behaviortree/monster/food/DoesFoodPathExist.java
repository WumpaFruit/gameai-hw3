package behaviortree.monster.food;

import main.GameState;
import behaviortree.core.LeafTask;

public class DoesFoodPathExist extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		return state.closestFoodPath != null;
	}

}
