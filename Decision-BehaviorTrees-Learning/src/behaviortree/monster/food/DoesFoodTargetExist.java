package behaviortree.monster.food;

import main.GameState;
import behaviortree.core.LeafTask;

public class DoesFoodTargetExist extends LeafTask {

	/**
	 * Need to compare positions in case another character ate the food we were targeting.
	 */
	@Override
	public boolean perform(GameState state) {
		boolean foodAndTargetExist = state.closestFood != null && state.closestFoodTarget != null;
		boolean foodSnatched = !state.availableFoodItems.contains(state.closestFood);
		
		if (!foodAndTargetExist || foodSnatched) {
			state.closestFood = null;
			state.closestFoodTarget = null;
			state.closestFoodPath = null;
			state.wanderTarget = null;
			state.wanderPath = null;
			return false;
		}
		
		return true;
	}

}
