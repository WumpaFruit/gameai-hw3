package behaviortree.monster.food;

import processing.core.PVector;
import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class GenerateFoodPath extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Generating food path";
		IndoorPathfinding.writeLine();
		
		state.closestFoodPath = IndoorPathfinding.pathfindAStar(state.character.getPosition(), new PVector(state.closestFood.getX(), state.closestFood.getY()));
		
		return state.closestFoodPath != null;
	}

}
