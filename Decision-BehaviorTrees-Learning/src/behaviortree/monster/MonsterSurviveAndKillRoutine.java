package behaviortree.monster;

import behaviortree.core.Selector;

public class MonsterSurviveAndKillRoutine extends Selector {
	public MonsterSurviveAndKillRoutine() {
		subTasks.add(new FoodSequencer());
		subTasks.add(new ChaseSequencer());
		subTasks.add(new WanderSequencer());
	}
}
