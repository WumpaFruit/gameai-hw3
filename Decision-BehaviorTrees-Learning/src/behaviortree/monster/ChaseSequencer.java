package behaviortree.monster;

import behaviortree.core.Sequencer;
import behaviortree.monster.chase.SeeHeroOrAlreadyHostile;
import behaviortree.monster.chase.SeekAndEatHeroSelector;

public class ChaseSequencer extends Sequencer {
	public ChaseSequencer() {
		subTasks.add(new SeeHeroOrAlreadyHostile());
		subTasks.add(new SeekAndEatHeroSelector());
	}
}
