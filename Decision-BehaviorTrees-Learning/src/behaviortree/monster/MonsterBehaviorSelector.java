package behaviortree.monster;

import behaviortree.core.Selector;
import behaviortree.monster.dance.MonsterDanceRoutine;

public class MonsterBehaviorSelector extends Selector {
	public MonsterBehaviorSelector() {
		subTasks.add(new MonsterDanceRoutine());
		subTasks.add(new MonsterSurviveAndKillRoutine());
	}
}
