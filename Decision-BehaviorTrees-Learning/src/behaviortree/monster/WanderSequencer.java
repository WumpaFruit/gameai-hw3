package behaviortree.monster;

import behaviortree.core.Sequencer;
import behaviortree.monster.wander.PathfollowWander;
import behaviortree.monster.wander.WanderPathSelector;
import behaviortree.monster.wander.WanderTargetSelector;

public class WanderSequencer extends Sequencer {
	public WanderSequencer() {
		subTasks.add(new WanderTargetSelector());
		subTasks.add(new WanderPathSelector());
		subTasks.add(new PathfollowWander());
	}
}
