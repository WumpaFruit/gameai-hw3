package behaviortree.monster.dance;

import main.Character;
import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.Composite;
import behaviortree.core.Task;

public class RepeatDecorator extends Composite {
	public static final long PAUSE_TIME = 500;
	public static final long DANCE_TIME = 5000;
	private boolean finished;
	
	public RepeatDecorator() {
		finished = false;
	}

	@Override
	public boolean perform(GameState state) {
		if (state.danceTimerSet()) {
			if (state.danceCompleted()) {
				state.status = "Done dancing";
				IndoorPathfinding.writeLine();
				state.monsterCelebrating = false;
				state.character.stopAngularMovement();
				IndoorPathfinding.monsterDoneDancing();	
				state.danceOrientationsGenerated = false;
				state.danceTimer = 0L;
				state.dancePauseTimer = 0L;
				state.hostile = false;			
				return false;
			}
			
			if (state.dancePauseTimerSet()) { //We are waiting for a second to pass
				if (state.dancePauseCompleted()) {//Move to next dancing position
					
					state.status = "Moving to next dancing position";
					IndoorPathfinding.writeLine();

					state.dancePauseTimer = 0L;
					state.run++;
					
					if (state.run >= subTasks.size()) {
						state.run = 0;
					}
					return true;
				} else {//Keep dancing, because a second hasn't passed yet
					Task t = subTasks.get(state.run);
					boolean subTaskSucceeded = t.perform(state);
					
					return true;
				}
			} else { //Set the timer for a second to pass
				/*Task t = subTasks.get(run);
				boolean subTaskSucceeded = t.perform(state);*/
				
				state.status = "Setting pause timer";
				IndoorPathfinding.writeLine();
				
				state.dancePauseTimer = System.currentTimeMillis();
			}
		} else {
			state.status = "Starting to dance";
			IndoorPathfinding.writeLine();
			state.danceTimer = System.currentTimeMillis();
		}

		return true;
	}
	
	public boolean isFinished() {
		return finished;
	}

}
