package behaviortree.monster.dance;

import main.GameState;
import behaviortree.core.LeafTask;

public class AreOrientationsGenerated extends LeafTask {
	
	private RepeatDecorator repeatDecorator;
	
	public AreOrientationsGenerated(RepeatDecorator repeatDecorator) {
		this.repeatDecorator = repeatDecorator;
	}

	@Override
	public boolean perform(GameState state) {
		return state.danceOrientationsGenerated;
	}

}
