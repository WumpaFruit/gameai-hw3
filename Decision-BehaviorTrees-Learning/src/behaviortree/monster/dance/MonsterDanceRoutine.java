package behaviortree.monster.dance;

import main.GameState;
import behaviortree.core.Sequencer;
import behaviortree.core.Task;

public class MonsterDanceRoutine extends Sequencer {
	RepeatDecorator repeatDecorator = new RepeatDecorator();
	
	public MonsterDanceRoutine() {
		subTasks.add(new HaveIWon());
		subTasks.add(new GenerateOrientationsSelector(repeatDecorator));
		subTasks.add(repeatDecorator);
	}
	
	public boolean perform(GameState state) {
		for (Task t: subTasks) {
			boolean subTaskSucceeded = t.perform(state);
			
			if (!subTaskSucceeded) {
				return false;
			}
		}
		
		return true;
	}
}
