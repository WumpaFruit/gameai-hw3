package behaviortree.monster.dance;

import main.GameState;
import behaviortree.core.LeafTask;

public class HaveIWon extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		return state.monsterCelebrating;
	}

}
