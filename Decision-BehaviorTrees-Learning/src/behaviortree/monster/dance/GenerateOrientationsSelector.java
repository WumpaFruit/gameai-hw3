package behaviortree.monster.dance;

import behaviortree.core.Selector;

public class GenerateOrientationsSelector extends Selector {
	
	public GenerateOrientationsSelector(RepeatDecorator repeatDecorator) {
		subTasks.add(new AreOrientationsGenerated(repeatDecorator));
		subTasks.add(new GenerateOrderOfOrientations(repeatDecorator));
	}
}
