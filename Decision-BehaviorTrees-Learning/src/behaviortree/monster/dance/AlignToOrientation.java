package behaviortree.monster.dance;

import main.Align;
import main.GameState;
import main.IndoorPathfinding;
import main.Steering;
import behaviortree.core.LeafTask;

public class AlignToOrientation extends LeafTask {
	private double targetOrientation;
	
	public AlignToOrientation(double targetOrientation) {
		this.targetOrientation = targetOrientation;
	}

	@Override
	public boolean perform(GameState state) {
		state.alignOrientationSteering = Align.getSteering(state.character, targetOrientation);
		
		if (state.alignOrientationSteering == null) {
			state.character.stopAngularMovement();
			state.status = "Arrived at target orientation";
			IndoorPathfinding.writeLine();
			return false;
		}
		
		state.status = "Moving toward dancing orientation";
		IndoorPathfinding.writeLine();
		
		state.steering.add(state.alignOrientationSteering);
		
		return true;
	}

}
