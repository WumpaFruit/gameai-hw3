package behaviortree.monster.dance;

import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class GenerateOrderOfOrientations extends LeafTask {
	
	private RepeatDecorator repeatDecorator;
	
	public GenerateOrderOfOrientations(RepeatDecorator repeatDecorator) {
		this.repeatDecorator = repeatDecorator;
	}

	@Override
	public boolean perform(GameState state) {
		double currentOrientation = state.character.getOrientation();
		
		state.status = "Generating dance orientations";
		IndoorPathfinding.writeLine();
		
		repeatDecorator.addTask(new AlignToOrientation(currentOrientation + (Math.PI / 2)));
		repeatDecorator.addTask(new AlignToOrientation(currentOrientation + Math.PI));
		repeatDecorator.addTask(new AlignToOrientation(currentOrientation + (3 * Math.PI / 2)));
		repeatDecorator.addTask(new AlignToOrientation(currentOrientation));
		
		state.danceOrientationsGenerated = true;
		
		return true;
	}

}
