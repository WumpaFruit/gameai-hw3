package behaviortree.monster.wander;

import behaviortree.core.Selector;

public class WanderPathSelector extends Selector {
	public WanderPathSelector() {
		subTasks.add(new DoesWanderPathExist());
		subTasks.add(new GenerateWanderPath());
	}
}
