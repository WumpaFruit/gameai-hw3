package behaviortree.monster.wander;

import main.GameState;
import behaviortree.core.LeafTask;

public class DoesWanderTargetExist extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		return state.wanderTarget != null;
	}

}
