package behaviortree.monster.wander;

import main.GameState;
import behaviortree.core.LeafTask;

public class DoesWanderPathExist extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		return state.wanderPath != null;
	}

}
