package behaviortree.monster.wander;

import behaviortree.core.Selector;

public class WanderTargetSelector extends Selector{
	public WanderTargetSelector() {
		subTasks.add(new DoesWanderTargetExist());
		subTasks.add(new GenerateWanderTarget());
	}
}
