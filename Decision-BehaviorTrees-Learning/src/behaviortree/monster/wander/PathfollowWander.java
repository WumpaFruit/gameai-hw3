package behaviortree.monster.wander;

import main.FollowPath;
import main.GameState;
import main.IndoorPathfinding;
import main.Steering;
import behaviortree.core.LeafTask;

public class PathfollowWander extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Trying to follow wander path";
		IndoorPathfinding.writeLine();
		
		state.pathFollowWanderSteering = FollowPath.getSteering(state.character, state.wanderPath);
		
		if (state.pathFollowWanderSteering != null) {
			state.steering.add(state.pathFollowWanderSteering);
			state.status = "On the prowl";
			IndoorPathfinding.writeLine();
		} else {	//If null, we have arrived at the target. Reset variables.
			state.status = "Arrived at wander target";
			IndoorPathfinding.writeLine();
			
			state.wanderTarget = null;
			state.wanderPath = null;
			state.closestFood = null;
			state.closestFoodTarget = null;
			state.closestFoodPath = null;
		}
		
		return state.pathFollowWanderSteering != null;
	}

}
