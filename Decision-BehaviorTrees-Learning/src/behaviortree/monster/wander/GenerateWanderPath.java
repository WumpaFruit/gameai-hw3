package behaviortree.monster.wander;

import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class GenerateWanderPath extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Generating wander path";
		IndoorPathfinding.writeLine();
		
		state.wanderPath = IndoorPathfinding.pathfindAStar(state.character.getPosition(), state.wanderTarget);
		
		return state.wanderPath != null;
	}

}
