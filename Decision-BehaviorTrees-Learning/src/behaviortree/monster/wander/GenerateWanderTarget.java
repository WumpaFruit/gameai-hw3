package behaviortree.monster.wander;

import main.Area;
import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class GenerateWanderTarget extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Generating wander target";
		IndoorPathfinding.writeLine();
		
		int currentArea = state.area.ordinal();
		int randAreaNum = state.rand.nextInt(18) + 1;
		
		while (randAreaNum == currentArea) {
			randAreaNum = state.rand.nextInt(18) + 1;
		}
		
		state.wanderTarget = Area.getRandomLocationInArea(Area.values()[randAreaNum]);
		state.wanderPath = null;
		
		return state.wanderTarget != null;
	}
	
}
