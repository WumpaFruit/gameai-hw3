package behaviortree.monster.wander;

import behaviortree.core.Selector;

public class PathfollowWanderSelector extends Selector {
	public PathfollowWanderSelector() {
		subTasks.add(new PathfollowWander());
		subTasks.add(new GenerateWanderTarget());
	}
}
