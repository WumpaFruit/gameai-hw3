package behaviortree.monster.chase;

import processing.core.PVector;
import main.GameState;
import behaviortree.core.LeafTask;

public class SeeHeroOrAlreadyHostile extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		if (state.hostile) {
			return true;
		}
		
		if (pointInTriangle(state.hero.getPosition(), state.character.getPosition(), state.perceptionWindowLeft, state.perceptionWindowRight)) {
			state.hostile = true;
			state.wanderTarget = null;
			state.wanderPath = null;
			state.closestFoodTarget = null;
			state.closestFoodPath = null;
			state.chaseTimer = System.currentTimeMillis();
			return true;
		}
		
		return false;
	}
	
	//From http://www.blackpawn.com/texts/pointinpoly/
	private boolean pointInTriangle(PVector p, PVector a, PVector b, PVector c) {
		return sameSide(p, a, b, c) && sameSide(p, b, a, c) && sameSide(p, c, a, b);
	}
	
	//From http://www.blackpawn.com/texts/pointinpoly/
	private boolean sameSide(PVector p1, PVector p2, PVector a, PVector b) {
		//PVector cp1 = null, cp2 = null;
		PVector cp1 = PVector.cross(PVector.sub(b, a), PVector.sub(p1, a), null);
		PVector cp2 = PVector.cross(PVector.sub(b, a), PVector.sub(p2, a), null);
		
		return PVector.dot(cp1, cp2) >= 0;
	}

}
