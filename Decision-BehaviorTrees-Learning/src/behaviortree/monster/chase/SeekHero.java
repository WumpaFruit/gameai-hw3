package behaviortree.monster.chase;

import processing.core.PVector;
import main.GameState;
import main.IndoorPathfinding;
import main.Seek;
import main.Steering;
import behaviortree.core.LeafTask;

public class SeekHero extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.seekHeroSteering = Seek.getSteering(state.character,
				state.hero.getPosition());

		if (state.seekHeroSteering == null || PVector.dist(state.character.getPosition(), state.hero.getPosition()) <= 2) {
			state.seekHeroSteering = null;
			return false;
		}

		state.steering.add(state.seekHeroSteering);
		state.status = "In pursuit";
		IndoorPathfinding.writeLine();

		return true;
	}
}