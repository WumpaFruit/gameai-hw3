package behaviortree.monster.chase;

import processing.core.PVector;
import main.GameState;
import main.IndoorPathfinding;
import behaviortree.core.LeafTask;

public class EatHero extends LeafTask {

	@Override
	public boolean perform(GameState state) {
		state.status = "Eating hero...delicious";
		IndoorPathfinding.writeLine();
		IndoorPathfinding.monsterWins();
		state.character.setVelocity(new PVector(0, 0));
		state.monsterCelebrating = true;
		return true;
	}

}
