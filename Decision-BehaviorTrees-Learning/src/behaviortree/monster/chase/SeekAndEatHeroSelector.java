package behaviortree.monster.chase;

import behaviortree.core.Selector;

public class SeekAndEatHeroSelector extends Selector {
	public SeekAndEatHeroSelector() {
		subTasks.add(new SeekHero());
		subTasks.add(new EatHero());
	}
}
