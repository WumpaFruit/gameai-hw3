package behaviortree.core;

import main.GameState;

public abstract class Task {
	protected boolean succeeded;
	
	public abstract boolean perform(GameState state);
	
	public boolean succeeded() {
		return succeeded;
	}
	
	public boolean failed() {
		return !succeeded;
	}
}