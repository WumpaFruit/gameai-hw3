package behaviortree.core;

import main.GameState;

public abstract class Sequencer extends Composite {
	
	public boolean perform(GameState state) {
		for (Task t: subTasks) {
			boolean subTaskSucceeded = t.perform(state);
			
			if (!subTaskSucceeded) {
				return false;
			}
		}
		
		return true;
	}
}