package behaviortree.core;

import main.GameState;

public abstract class Selector extends Composite {
	
	public boolean perform(GameState state) {
		for (Task t: subTasks) {
			boolean subTaskSucceeded = t.perform(state);
			
			if (subTaskSucceeded) {
				return true;
			}
		}
		
		return false;
	}
}