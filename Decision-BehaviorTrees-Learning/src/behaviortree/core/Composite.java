package behaviortree.core;

import java.util.ArrayList;

public abstract class Composite extends Task {
	protected ArrayList<Task> subTasks;
	
	public Composite() {
		subTasks = new ArrayList<>();
	}
	
	public void addTask(Task t) {
		subTasks.add(t);
	}
	
	public int numSubTasks() {
		return subTasks.size();
	}
}