Decision Trees, Behavior Trees, and Learning README
By Joseph Sankar

Decision Trees, Behavior Trees, and Learning contains two programs that each show the required behaviors of running AI characters with decision and behavior trees and learning decision trees from the state information outputted from the monster's behavior tree run. If you run into any issues with the code, contact me at jesankar@ncsu.edu.

1. Installing into Eclipse
	a. After downloading Decision-BehaviorTrees-Learning.zip, open Eclipse.
	b. Click File --> Import.
	c. Under "General", choose "Existing Projects into Workspace".
	d. Choose "Select archive file" and browse to the file you downloaded and click Next.
	e. You should see a "Decision-BehaviorTrees-Learning" project. Make sure it's selected and click Finish. You should now see the project in your Eclipse workspace.

2. Running IndoorPathfinding (running decision and behavior trees of the hero and monster)
	a. Right-click IndoorPathfinding in the main package.
	b. Select Run As --> Java Application to run the program. You should see the hero and monster behaviors in a 800x800 fullscreen window.
	c. Press ESC to quit.
	
3. Running DecisionTreeLearningRunner (running the decision tree learning algorithm)
	a. Right-click DecisionTreeLearningRunner in the decisiontreelearning package.
	b. Select Run As --> Java Application to run the program. You should see console output that represents the generated decision tree. Unfortunately this program does not automatically generate the decision tree. The output needs to manually be converted to a decision tree using if-else statements. However, a decision tree has already been generated and implemented based on the "outputFinal.txt" file that should be in the root directory of the project.
	
4. Configuring IndoorPathfinding
	You can configure IndoorPathfinding to run the decision tree or behavior tree for the monster. In order to do this, open IndoorPathfinding in the main package and change the RUN_MONSTER_BEHAVIOR_TREE constant to true to run the behavior tree and false to run the decision tree. If you run the decision tree, you may want to turn off logging by changing the LOG_OUTPUT constant to false. You can change the name of the output file by editing the OUTPUT_FILENAME constant. You may also run two monsters, one running the behavior tree and another running the generated decision tree, by setting TWO_MONSTERS to true.

5. Configuring DecisionTreeLearningRunner
	You can configure DecisionTreeLearningRunner by changing the FILENAME constant to the desired output file that was previously generated from IndoorPathfinding.